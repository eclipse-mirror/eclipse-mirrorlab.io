# Inofficial Mirror of some Eclipse Update Sites plus Maven Repository

| Original Update Site                             | Mirrored Update Site                                      | Maven Repository                                          |
|--------------------------------------------------|-----------------------------------------------------------|-----------------------------------------------------------|
| http://download.eclipse.org/releases/2018-09/    | http://eclipse-mirror.gitlab.io/eclipse-releases-2018-09/ | https://gitlab.com/api/v4/projects/9009533/packages/maven |
| http://download.eclipse.org/eclipse/updates/4.9/ | http://eclipse-mirror.gitlab.io/eclipse-updates-4.9/      | https://gitlab.com/api/v4/projects/9009247/packages/maven |

See https://docs.gitlab.com/ee/user/project/packages/maven_repository.html how to use the maven repository.